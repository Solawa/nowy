#include "bomb.h"
#include "flame.h"
#include <QTimer>
#include <QGraphicsScene>
#include"settings.h"

Bomb::Bomb(int x, int y)
{
    x_ = x;
    y_ = y;
    setRect(x*sizes::FieldSize,y*sizes::FieldSize,20,20);
    setBrush(QColor(Qt::red));

    QTimer * timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(tictac()));
    timer->start(1000);

}

void Bomb::tictac()
{
    emit boom(this);
    scene()->removeItem(this);
    delete this;
}
