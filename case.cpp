#include <QGraphicsScene>
#include "settings.h"

#include "case.h"

Case::Case(int x, int y)
{
    this->flame=nullptr;
    setRect(x*sizes::FieldSize,y*sizes::FieldSize, sizes::FieldSize, sizes::FieldSize);
    setBrush(QColor(Qt::black));
}
