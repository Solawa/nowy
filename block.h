#ifndef FIELD_H
#define FIELD_H

#include <QObject>
#include <QGraphicsRectItem>
#include "bomb.h"
#include "flame.h"

class Block:public QObject, public QGraphicsRectItem
{
public:
    Block() = default;
    Bomb * bomb;
    Flame* flame;
    void boomField();
};


#endif // FIELD_H
