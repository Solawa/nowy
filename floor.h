#ifndef BLOCK_H
#define BLOCK_H
#include "block.h"
#include "flame.h"

class Floor: public Block
{
public:
    Floor(int x, int y);
};

#endif // BLOCK_H
