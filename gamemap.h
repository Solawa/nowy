#ifndef GAMEMAP_H
#define GAMEMAP_H

#include "field.h"
#include <vector>


class GameMap:public Field
{
public:
    GameMap(int n);
    std::vector<std::vector<Field * >> fields;
};

#endif // GAMEMAP_H
