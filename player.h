#ifndef MYRECT_H
#define MYRECT_H

#include <QGraphicsPathItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QObject>
#include "block.h"
#include "flame.h"
#include "field.h"
#include "settings.h"
#include "helath.h"

class Player: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    int x;
    int y;
    Helath* healthPlayer;
    Player(int x, int y);
    void decreaseHealth();
    void plant(std::vector<std::vector<Block * >> fields);
    void makeMove(int x, int y, Field*);
    bool busySpot(int, int, Player*);
    int getX();
    int getY();
signals:
    void death(Player*);
};

#endif // MYRECT_H
