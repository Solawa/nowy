#include "helath.h"
#include <QFont>

Helath::Helath()
{
    health = 3;

    setPlainText(QString("Lives: ") + QString::number(health));
    setDefaultTextColor(Qt::red);
    setFont(QFont("times", 16));
}

void Helath::decrease()
{
    health--;
    setPlainText(QString("Lives: ") + QString::number(health));
}
