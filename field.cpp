#include "field.h"

Field::Field(Floor* block, Block* obst)
{
    this->block = block;
    this->field = obst;
    this->flame = nullptr;
}

void Field::deleteFlame()
{
    this->flame = nullptr;
}
