#include <QGraphicsScene>
#include "settings.h"

#include "floor.h"


Floor::Floor(int x, int y):Block()
{
    setRect(x*sizes::FieldSize,y*sizes::FieldSize, sizes::FieldSize, sizes::FieldSize);
    setBrush(QColor(Qt::green));
}
