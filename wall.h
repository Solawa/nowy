#ifndef WALL_H
#define WALL_H

#include "block.h"

class Wall: public Block
{
public:
    Wall(int x, int y);
};


#endif // WALL_H
