#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "block.h"

class Case : public Block
{
public:
    Case(int x, int y);
};

#endif // OBSTACLE_H
