#ifndef FLAME_H
#define FLAME_H

#include <QObject>
#include <QGraphicsRectItem>


class Flame:public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    int x_;
    int y_;
    Flame(int x, int y);

public slots:
    void tictac();
signals:
    void boom(Flame *);
    void deleteFlame();
};

#endif // FLAME_H
