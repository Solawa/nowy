#include <QRandomGenerator>
#include <QKeyEvent>
#include <game.h>


Game::Game()
{
    freq = 3;
    gameColumns = sizes::GameWidth/sizes::FieldSize;
    gameRow = sizes::GameHeight/sizes::FieldSize;

    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,sizes::GameWidth,sizes::GameHeight);

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(sizes::GameWidth,sizes::GameHeight);


    makeMap();

    player1 = new Player(1, 1);
    player1->setFlag(QGraphicsItem::ItemIsFocusable);
    player1->setFocus();
    scene->addItem(player1);
    scene->addItem(player1->healthPlayer);

    player2 = new Player(gameColumns-2, gameRow-2);
    scene->addItem(player2);
    scene->addItem(player2->healthPlayer);
    connect(player1, SIGNAL(death(Player*)), this, SLOT(endGame(Player*)));
    connect(player2, SIGNAL(death(Player*)), this, SLOT(endGame(Player*)));
}

void Game::makeBoom(Bomb* bomb)
{
    unsigned int x_ = static_cast<unsigned int>(bomb->x_);
    unsigned int y_ = static_cast<unsigned int>(bomb->y_);
    destroyField(x_+1, y_);
    destroyField(x_-1, y_);
    destroyField(x_, y_-1);
    destroyField(x_, y_+1);
    destroyField(x_, y_);
}

void Game::destroyField(int x_, int y_)
{
    Field* field = fields[y_][x_];
    //Checks if block is destroyable
    if (nullptr != dynamic_cast<Case *>(field->field))
    {
        scene->removeItem(field->field);
        delete field->field;
        fields[y_][x_]->field = nullptr;
    }

    //addFlame if appropriate block
    if(nullptr == dynamic_cast<Wall *>(field->field))
    {
        Flame* flame = new Flame(x_,y_);
        field->flame = flame;
        scene->addItem(flame);
        playerOnFlame(player1, flame);
        playerOnFlame(player2, flame);
        connect(flame, SIGNAL(deleteFlame()), field, SLOT(deleteFlame()));
    }



}

void Game::makeMap()
{
    QRandomGenerator *generator = new QRandomGenerator(static_cast<quint32>(time(nullptr)));

    for(int i=0; i<gameRow ; i++)
    {
        std::vector<Field *> temp;
        for(int j=0; j<gameColumns ; j++)
        {
            Field * field = new Field(new Floor(j,i));
            scene->addItem(field->block);
            //adding Walls(undestructable)
            if(j==0  || i == 0 ||
               j == gameColumns-1 || i == gameRow-1 ||
              (i%2 == 0 && j%2 == 0) ){
                field->field = new Wall(j,i);
                scene->addItem(field->field);
            }
            else if ((i > 2 || j > 2) &&
                     (i < gameRow - 3 || j < gameColumns-3)) {
                    if(generator->bounded(freq)) {
                        field->field = new Case(j,i);
                        scene->addItem(field->field);
                    }
            }

            temp.push_back(field);

        }
        fields.push_back(temp);
    }
}

bool Game::canMove(int x, int y, Player* comp)
{
    if(x == comp->x && y == comp->y)
        return false;

    if(x < gameColumns && x > 0){
        if(y < gameRow && y >0){
            if(fields[y][x]->field == nullptr)
            {
                return true;
            }
        }
    }

    return false;
}

void Game::keyPressEvent(QKeyEvent *event)
{
    int x1 = player1->x;
    int y1 = player1->y;
    //first player1
    switch (event->key()) {
    case Qt::Key_Right:
        if(canMove(x1+1, y1, player2))
            player1->makeMove(x1+1, y1, fields[y1][x1+1]);
        break;
    case Qt::Key_Down:
        if(canMove(x1, y1+1, player2))
            player1->makeMove(x1, y1+1, fields[y1+1][x1]);
        break;
    case Qt::Key_Left:
        if(canMove(x1-1, y1, player2))
            player1->makeMove(x1-1, y1, fields[y1][x1-1]);
        break;
    case Qt::Key_Up:
        if(canMove(x1, y1-1, player2))
            player1->makeMove(x1, y1-1, fields[y1-1][x1]);
        break;
    case Qt::Key_0:
        Bomb * bomb = new Bomb(x1,y1);
        scene->addItem(bomb);
        fields[y1][x1]->bomb = bomb;
        connect(bomb, SIGNAL(boom(Bomb*)), this, SLOT(makeBoom( Bomb*)));
        break;
    }

    //second player
    int x2 = player2->x;
    int y2 = player2->y;
    switch (event->key()) {
    case Qt::Key_D:
        if(canMove(x2+1, y2, player2))
            player2->makeMove(x2+1, y2, fields[y2][x2+1]);
        break;
    case Qt::Key_S:
        if(canMove(x2, y2+1, player2))
            player2->makeMove(x2, y2+1, fields[y2+1][x2]);
        break;
    case Qt::Key_A:
        if(canMove(x2-1, y2, player2))
            player2->makeMove(x2-1, y2, fields[y2][x2-1]);
        break;
    case Qt::Key_W:
        if(canMove(x2, y2-1, player2))
            player2->makeMove(x2, y2-1, fields[y2-1][x2]);
        break;
    case Qt::Key_Space:
        Bomb * bomb = new Bomb(x2,y2);
        scene->addItem(bomb);
        fields[y2][x2]->bomb = bomb;
        connect(bomb, SIGNAL(boom(Bomb*)), this, SLOT(makeBoom( Bomb*)));
        break;
    }
}

void Game::playerOnFlame(Player * player, Flame * flame)
{
    if(player->x == flame->x_ && player->y == flame->y_)
        player->decreaseHealth();
}

void Game::endGame(Player* player)
{
    delete this;
}

