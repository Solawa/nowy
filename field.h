#ifndef FLOOR_H
#define FLOOR_H

#include <QObject>
#include <QGraphicsRectItem>
#include "bomb.h"
#include "flame.h"
#include "block.h"
#include "floor.h"
#include "case.h"
#include "wall.h"

class Field:public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Field(Floor* block = nullptr, Block* field = nullptr);
    Bomb * bomb;
    Flame* flame;

    //Block, can be destructible(Box) or undestructible(Wall)
    Block* field;
    Floor* block;
    void boomField();
public slots:
    void deleteFlame();
};



#endif // FLOOR_H
