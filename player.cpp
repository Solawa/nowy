#include "player.h"
#include "bomb.h"
#include "block.h"
#include "settings.h"
#include <QKeyEvent>
#include <QGraphicsScene>


Player::Player(int x_, int y_)
{
    x=x_;
    y=y_;
    healthPlayer = new Helath();
    setPixmap(QPixmap(":/images/char.png"));
    this->setPos(x*sizes::FieldSize,y*sizes::FieldSize);
    healthPlayer->setPos(x*sizes::FieldSize, sizes::GameHeight-sizes::FieldSize);
}

void Player::decreaseHealth()
{
    healthPlayer->decrease();
    if(healthPlayer->health == 0)
    {
        emit death(this);
        delete this;
    }

}


void Player::plant(std::vector<std::vector<Block * >> fields)
{
    Bomb * bomb = new Bomb(x,y);
    scene()->addItem(bomb);
    fields[static_cast<unsigned int>(y)][static_cast<unsigned int>(x)]->bomb = bomb;
}

void Player::makeMove(int x, int y, Field* field)
{
    this->setPos(x*sizes::FieldSize,y*sizes::FieldSize);
    this->x =x;
    this->y =y;
    if(nullptr != field->flame)
        this->decreaseHealth();
    //qDebug() << "nie boli";

}

bool Player::busySpot(int x, int y, Player * comp)
{
    return (x == comp->x && y == comp->y);
}

int Player::getX()
{
    return this->x;
}

int Player::getY()
{
    return this->y;
}
