#ifndef GAME_H
#define GAME_H

#include <QGraphicsPathItem>
#include <QObject>
#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <vector>
#include <QMainWindow>
#include "player.h"
#include "block.h"
#include "bomb.h"
#include "settings.h"
#include "field.h"

class Game:public QGraphicsView{
    Q_OBJECT
public:
    Game();
    void destroyField(int x, int y);
    void makeMap();
    bool canMove(int x, int y, Player*);
    void keyPressEvent(QKeyEvent* event);
    void playerOnFlame(Player*, Flame*);

    QGraphicsScene * scene;

    Player* player1;
    Player* player2;

    std::vector<std::vector<Field * >> fields;

public slots:
    void makeBoom(Bomb* bomb);
    void endGame(Player *);

private:
    int gameRow;
    int gameColumns;
    int freq;

};

#endif // GAME_H
