#ifndef SETTINGS_H
#define SETTINGS_H

namespace keyAct {

    enum action {
        Left,
        Right,
        Up,
        Down,
        Plant

};

}

namespace sizes {

    enum gameStats {
        GameWidth = 600,
        GameHeight = 520,
        FieldSize = 40,
        Players = 2
    };
    enum playerStats {
        Bombs = 1,
        ExplosionRange = 1,
        Health = 3
    };
}

#endif // SETTINGS_H
