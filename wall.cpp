#include <QGraphicsScene>
#include "wall.h"
#include "settings.h"


Wall::Wall(int x, int y)
{
    this->flame=nullptr;
    setRect(x*sizes::FieldSize,y*sizes::FieldSize, sizes::FieldSize, sizes::FieldSize);
    setBrush(QColor(Qt::gray));
}
