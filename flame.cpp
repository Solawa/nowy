#include "flame.h"
#include "settings.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QColor>

Flame::Flame(int x, int y)
{
    x_ = x;
    y_ = y;
    setRect(x*sizes::FieldSize,y*sizes::FieldSize,20,20);
    setBrush(QColor(Qt::yellow));

    QTimer * timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(tictac()));
    timer->start(600);

}
void Flame::tictac()
{
    emit boom(this);
    scene()->removeItem(this);
    emit deleteFlame();
    delete this;
}

