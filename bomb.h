#ifndef BOMB_H
#define BOMB_H

#include <QGraphicsRectItem>
#include <QObject>
#include "bomb.h"

class Bomb:public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    int x_;
    int y_;
    Bomb(int x, int y);
public slots:
    void tictac();
signals:
    void boom(Bomb *);
};

#endif // BOMB_H
